﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SceneInformation //cant be static when we need to find the obstacles of every single user individually
{

    public static GameObject[] dynamicObstacles;
    public static GameObject[] staticObstacles;

    
    // Start is called before the first frame update
    public static void FindObstacles()
    {
        dynamicObstacles = GameObject.FindGameObjectsWithTag("dynamic obstacle"); 
        // if (dynamicObstacles == null)
        //     {
        //         print("no dynamic obstacles found");
        //     }
        Debug.Log(dynamicObstacles.Length + " dynamic obstacles found");
        staticObstacles = GameObject.FindGameObjectsWithTag("static obstacle");
        // if (staticObstacles == null)
        //     {
        //         print("no static obstacles found");
        //     }
        Debug.Log(staticObstacles.Length + " static obstacles found");


    }
}
