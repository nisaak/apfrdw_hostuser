﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(EvaluateField))]
public class EvaluateFieldEditor : Editor
{
    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();

        EvaluateField myTarget = (EvaluateField)target;

        if (GUILayout.Button("Regenerate Static Field"))
        {
            myTarget.regenerate = true;
        }

        if(GUILayout.Button("Research obstacles"))
        {
            myTarget.SetObstaclesArray();
        }
        
    }
}