﻿using System;
using System.Collections;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RedirectionVector))]
public class RedirectionVectorEditor : Editor
{
    public override void OnInspectorGUI()
    {

        DrawDefaultInspector();

        RedirectionVector myTarget = (RedirectionVector)target;

        if (GUILayout.Button("Research Tag List"))
        {
            myTarget.createTagList();
        }


    }
}