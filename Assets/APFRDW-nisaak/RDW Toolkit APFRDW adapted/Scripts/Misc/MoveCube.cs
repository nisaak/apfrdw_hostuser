﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// simple script to move a cube around to check in dynamic field
/// </summary>
public class MoveCube : MonoBehaviour
{
    // Start is called before the first frame update
    Vector3 _startPosition;
    public GameObject newParent;
    Vector3 offset;


    void Start()
    {
        offset = new Vector3(0.0f, 0.0f, 0.0f);
        transform.SetParent(newParent.transform);
        _startPosition = newParent.transform.position - offset;
    }

    // Update is called once per frame
    void Update()
    {
        transform.localPosition = _startPosition + new Vector3(Mathf.Sin(Time.realtimeSinceStartup*0.5f),0f , Mathf.Sin(Time.realtimeSinceStartup * 0.5f)) * 2.0f;
    }
}
