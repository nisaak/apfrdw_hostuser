﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Redirection;
using System.Linq;

/// <summary>
/// this class simply handels the storage and creation of potential fields 
/// </summary>
public class PotentialField 
{
    //Initialise variables
    public int resolution;
    public int gridsize_x;
    public int gridsize_z;
    public string type; //dynamic or static

    public float maxValue, minValue;


    public float[,] potentialfield;
    //float[,] normalizedPotentialfield;

    //Constructor
    public PotentialField(int _resolution, float _bounds_x, float _bounds_z, string _type)
    {
        this.resolution = _resolution;
        this.gridsize_x = Mathf.RoundToInt(_bounds_x * resolution);
        this.gridsize_z = Mathf.RoundToInt(_bounds_z * resolution);
        this.type = _type;

        potentialfield = new float[gridsize_x, gridsize_z];


    }

    //method to clear all array entries
    public void Empty()
    {
        for (int i = 0; i < gridsize_x; i++)
        {
            for (int j = 0; j < gridsize_z; j++)
            {
                potentialfield[j, i] = 0f;
            }
        }
    }

    //Obsolete
    public float GetRoundingError()
    {
        return 0f;
    }

}
