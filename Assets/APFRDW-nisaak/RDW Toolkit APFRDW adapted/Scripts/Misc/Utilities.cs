﻿using UnityEngine;
using System.Collections;
using System.Linq;

namespace Redirection
{
    /// <summary>
    /// Utilities class to handle all relevant utilities functions.
    /// </summary>
    public static class Utilities
    {

        public static Vector3 FlattenedPos3D(Vector3 vec, float height = 0)
        {
            return new Vector3(vec.x, height, vec.z);
        }

        public static Vector2 FlattenedPos2D(Vector3 vec)
        {
            return new Vector2(vec.x, vec.z);
        }

        public static Vector3 FlattenedDir3D(Vector3 vec)
        {
            return (new Vector3(vec.x, 0, vec.z)).normalized;
        }

        public static Vector2 FlattenedDir2D(Vector3 vec)
        {
            return new Vector2(vec.x, vec.z).normalized;
        }

        public static Vector3 UnFlatten(Vector2 vec, float height = 0)
        {
            return new Vector3(vec.x, height, vec.y);
        }

        /// <summary>
        /// Gets angle from prevDir to currDir in degrees, assuming the vectors lie in the xz plane (with left handed coordinate system).
        /// </summary>
        /// <param name="currDir"></param>
        /// <param name="prevDir"></param>
        /// <returns></returns>
        public static float GetSignedAngle(Vector3 prevDir, Vector3 currDir)
        {
            return Mathf.Sign(Vector3.Cross(prevDir, currDir).y) * Vector3.Angle(prevDir, currDir);
        }

        public static Vector3 GetRelativePosition(Vector3 pos, Transform _origin)
        {
            return Quaternion.Inverse(_origin.rotation) * (pos - _origin.position);
        }

        public static Vector3 GetRelativeDirection(Vector3 dir, Transform origin)
        {
            return Quaternion.Inverse(origin.rotation) * dir;
        }

        // Based on: http://stackoverflow.com/questions/4780119/2d-euclidean-vector-rotations
        // FORCED LEFT HAND ROTATION AND DEGREES
        public static Vector2 RotateVector(Vector2 fromOrientation, float thetaInDegrees)
        {
            Vector2 ret = Vector2.zero;
            float cos = Mathf.Cos(-thetaInDegrees * Mathf.Deg2Rad);
            float sin = Mathf.Sin(-thetaInDegrees * Mathf.Deg2Rad);
            ret.x = fromOrientation.x * cos - fromOrientation.y * sin;
            ret.y = fromOrientation.x * sin + fromOrientation.y * cos;
            return ret;
        }

        public static bool Approximately(Vector2 v0, Vector2 v1)
        {
            return Mathf.Approximately(v0.x, v1.x) && Mathf.Approximately(v0.y, v1.y);
        }

        public static float[,] ScaleArray(float[,] m) //TODO scaling needs to be better to account for high values near boundaries, TODO this takes up a lot of runtime
        {
            //float[] matrixMaxMin = new float[3];

            float max = float.MinValue;
            float min = float.MaxValue;

            for (int i = 0; i < m.GetLength(0); i++) //find max and min values
            {
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    //if (float.IsNegativeInfinity(m[i,j])){
                    //    m[i, j] = float.MinValue;
                    //}
                    //if (float.IsPositiveInfinity(m[i, j]))
                    //{
                    //    m[i, j] = float.MaxValue;
                    //}
                    if (m[i, j] > max)
                    {
                        max = m[i, j];
                        if (max >= float.MaxValue) max = float.MaxValue;
                    }
                    if (m[i, j] < min)
                    {
                        min = m[i, j];
                        if (min <= float.MinValue) min = float.MinValue;
                    }

                }
            }
            //Debug.Log((max, min));


            for (int i = 0; i < m.GetLength(0); i++) //normalize to 0 - 1
            {
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    m[i, j] = (m[i, j] - min) / (max - min);
                }
            }
            //matrixMaxMin[0] = m;
            //matrixMaxMin[1] = max;
            //matrixMaxMin[2] = min;


            return m;

        } //function to scale array 0-1

    }
}