﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Redirection;
using System.Linq;
using System.IO;

public class EvaluateField : MonoBehaviour
{
    //Declare the three field types
    public PotentialField staticField;
    public PotentialField dynamicField;
    public PotentialField fullField;

    //Reference to redirectionManager
    RedirectionManager redirectionManager;
    GameObject trackedSpace;
    GameObject texturePlane;

    //Declare Array of obstacles, maybe better to use List?
    public GameObject[] dynamicObstacles;
    public GameObject[] staticObstacles;
    GameObject[] obstacles;

    [Range(0.01f,1f)]
    public float staticGain;
    [Range(0.01f, 2f)]
    public float pow = 0.25f    ;
 
    [HideInInspector]
    public bool regenerate = false;

    public enum Quality { _default, low, medium, high };
    public Quality quality;

    public bool evaluateBorders;

    public Settings settings;
    
    //For resolution changes
    int resolution_saved;

    Bounds bounds;

    //Class to hold settings variables
    [Serializable]
    public class Settings
    {
        public int resolution;
        public float max_dist;
        [Range(1.0f, 3.0f)]
        public float dynamic_falloff, static_falloff;
    }

    //Get values from settings JSON file
    public static Settings CreateFromJSON(string jsonstring)
    {
        return JsonUtility.FromJson<Settings>(jsonstring);
    }

    //Method to add static and dynamic field
    public void AddFields()
    {
        for (int i = 0; i < staticField.gridsize_x; i++)
        {
            for (int j = 0; j < staticField.gridsize_z; j++)
            {
                fullField.potentialfield[i, j] = dynamicField.potentialfield[i, j] + staticField.potentialfield[i, j]; //add fields
            }
        }
    }


    /// <summary>
    /// This method iterates through all grid cells and evaluates it with the method EvalGridPoint at the specified grid cells
    /// </summary>
    /// <param name="potentialfield"></param>
    /// <param name="settings"></param>
    public void EvalFull(PotentialField potentialfield, Settings settings)
    {
        for (int i = 0; i < potentialfield.gridsize_x; i++)
        {
            for (int j = 0; j < potentialfield.gridsize_z; j++)
            {
                EvalGridPoint(i, j, potentialfield, settings); //compute potential value for every point in map
            }
        }
    }

    /// <summary>
    /// This evaluates to APF at a gridpoint specified by its location x,z. We can specify for which potentialfield (and what type it is) it is evaluated. An offset is added, so it's evaluated at the grid cells center.
    /// </summary>
    /// <param name="_x"></param>
    /// <param name="_z"></param>
    /// <param name="potentialfield"></param>
    /// <param name="settings"></param>
    public void EvalGridPoint(int _x, int _z, PotentialField potentialfield, Settings settings) //see Real-Time Obstacle Avoidance Using Harmonic Potential Function
    {
        //because we wanna evaluate in the middle of the cell so to speak, to avoid floating point errors
        float x = _x + 0.5f; 
        float z = _z + 0.5f;

        //currPos is the grid cells location in Unity space, relative to the tracked space
        Vector3 currPos = new Vector3(x / potentialfield.gridsize_x - 0.5f, 0.1f, z / potentialfield.gridsize_z - 0.5f); //vector -.5f to .5f relative to texture plane
        currPos = new Vector3(currPos.x * redirectionManager.trackedSpaceSizeX, 0.1f, currPos.z * redirectionManager.trackedSpaceSizeZ);

        //Iterate through all obstacles and add values
        if (potentialfield.type == "static") {
            foreach(GameObject obstacle in staticObstacles)
            {
                if (obstacle.activeSelf)
                {
                    Vector3 r = currPos - obstacle.transform.localPosition; //TODO FIx this
                    float dist = r.magnitude;
                    potentialfield.potentialfield[_x, _z] += staticGain * 1f / Mathf.Pow(dist, pow);


                }

            }
        }
        if (potentialfield.type == "dynamic")
        {
            foreach (GameObject obstacle in dynamicObstacles)
            {
                if (obstacle.activeSelf)
                {
                    Vector3 r = currPos - obstacle.transform.position; //TODO fix this value for dynamics obstacles
                    float dist = r.magnitude;
                    potentialfield.potentialfield[_x, _z] += 1 / Mathf.Pow(dist,pow);
                }

            }
        }
        else
            return;
        
        //The following lines of code check if the values are too high or low to be used
        if (!float.IsInfinity(potentialfield.potentialfield[_x, _z]))
            return;
        else
        {
            if (float.IsNegativeInfinity(potentialfield.potentialfield[_x, _z]))
            {
                potentialfield.potentialfield[_x, _z] = -200f;//float.MinValue;
            }
            if (float.IsPositiveInfinity(potentialfield.potentialfield[_x, _z]))
            {
                potentialfield.potentialfield[_x, _z] = 200f;// float.MaxValue;
            }
        }


    }

    //To change the resolution quality
    void ResolutionSettings()
    {
        switch (quality)
        {
            case (Quality._default):
                {
                    settings.resolution = 10;
                    break;
                }
            case (Quality.low):
                {
                    settings.resolution = 5;
                    break;
                }
            case (Quality.medium):
                {
                    settings.resolution = 10;
                    break;
                }
            case (Quality.high):
                {
                    settings.resolution = 20;
                    break;
                }
        }
    }

    /// <summary>
    /// This method searches for all existing obstacles, while differentiating between dynamic and static. It then initialises the obstacles array with the found obstacles.
    /// </summary>
    public void SetObstaclesArray()
    {
        dynamicObstacles = GameObject.FindGameObjectsWithTag("dynamic obstacle"); //new obstacles added to the scene after awake are currently not considered, FIX! 
        if (dynamicObstacles == null)
        {
            print("no dynamic obstacles found");
        }

        //print(dynamicObstacles.Length + " dynamic obstacles found");
        staticObstacles = GameObject.FindGameObjectsWithTag("static obstacle");
        if (staticObstacles == null)
        {
            print("no static obstacles found");
        }

        //print(staticObstacles.Length + " static obstacles found");
    }

    //Obsolete
    public Vector2Int getCurrentGridPos()
    {
        Vector3 currLocalPosition = redirectionManager.headTransform.localPosition;
        Vector2Int gridPosition = new Vector2Int(Mathf.RoundToInt(currLocalPosition.x * staticField.resolution + (staticField.gridsize_x / 2.0f)), Mathf.RoundToInt(currLocalPosition.z * staticField.resolution + (staticField.gridsize_z / 2.0f)));
        return gridPosition;
    }
    private void Awake() //gather information about scene
    {
        redirectionManager = GetComponentInParent<RedirectionManager>();

        trackedSpace = GameObject.Find("Tracked Space");
        texturePlane = GameObject.FindGameObjectWithTag("floor");

        bounds = this.gameObject.GetComponent<Renderer>().bounds;


}


    public void Start()
    {
        string path = Application.dataPath + "/APFRDW-nisaak/RDW Toolkit Template/Scripts/Misc/Settings/settings.json";
        string JSONString = File.ReadAllText(path);
        settings = CreateFromJSON(JSONString);

        staticGain = 1f;

        resolution_saved = settings.resolution;

        //Create new static, dynamic and full field
        staticField = new PotentialField(settings.resolution, bounds.size.x, bounds.size.z, "static");

        dynamicField = new PotentialField(settings.resolution, bounds.size.x, bounds.size.z, "dynamic");
        fullField = new PotentialField(settings.resolution, bounds.size.x, bounds.size.z, "hybrid");
        //EvalFull(staticField, settings);

        //SetObstaclesArray(); //sets the arrays for all obstacles and can be called from a different script if needed (updated)
    }

    public void Update()
    {
        ResolutionSettings();

        //Debug.Log(staticObstacles.Length);

        //Update fields if resolution is changed
        if (resolution_saved != settings.resolution)
        {
            staticField = new PotentialField(settings.resolution, bounds.size.x, bounds.size.z, "static");
            dynamicField = new PotentialField(settings.resolution, bounds.size.x, bounds.size.z, "dynamic");
            fullField = new PotentialField(settings.resolution, bounds.size.x, bounds.size.z, "hybrid");
            EvalFull(staticField, settings);
            resolution_saved = settings.resolution;
        }

        //Evaluate the dynamic field in Update
        EvalFull(dynamicField, settings);

        if (regenerate == true)
        {
            Array.Clear(staticField.potentialfield, 0, staticField.potentialfield.Length);
            EvalFull(staticField, settings);
            regenerate = false;
        }

        AddFields(); //this adds the static and dynamic potential field

        //Normalise the complete potential field
        Utilities.ScaleArray(fullField.potentialfield); //this function scales the final potential field to 0-1

        //Clear the dynamic field, so the values dont accumulate
        Array.Clear(dynamicField.potentialfield, 0, dynamicField.potentialfield.Length);

    }
}
