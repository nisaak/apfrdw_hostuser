﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Redirection;

/// <summary>
/// this resetter uses the redirection vector from the potential field to adjust the gains accordingly
/// so that the user is redirected in the correct direction.
/// For more info, see S2G, Thomas et al. https://illusioneering.cs.umn.edu/papers/thomas-vr2019.pdf
/// </summary>
public class APFResetter : Resetter
{

    private Transform prefabHUD = null;
    Transform instanceHUD;

    public Vector3 resetVector = Vector3.zero;
    float resetDirection;

    float overallInjectedRotation;

    //bools to stop values from changing during reset
    bool resetVectorset = false;
    bool resetAngleset = false; 
    public float resetTurnGain;

    public float resetAngle;


    //Checks four conditions and returns true if reset is required
    public override bool IsResetRequired() 
    {
        return ((IsUserOutOfBounds() && !isUserFacingAwayFromWall()) ^ (IsObstacleTooClose() && !isUserFacingAwayFromNearestObstacle()));
    }

    //Initialises reset by getting the required reset angle and direction of reorientation
    public override void InitializeReset()
    {
        GetResetAngleAndDirection();

        overallInjectedRotation = 0;

        //Sets HUD for task prompt
        SetHUD();
    }

    /// <summary>
    /// Keeps track of the resetAngle and the already completed rotation.
    /// </summary>
    public override void ApplyResetting()
    {
        //If statement only if resetAngle hasnt been set yet to avoid redundancy
        //Because we want to reorientate around reflex angle, we have to adjust resetAngle like this.
        if (resetAngleset == false)
        {
            if (resetAngle < 0)
            {
                resetAngle = 360 + resetAngle;
            }
            else
            {
                resetAngle = 360 - resetAngle;
            }
            resetAngleset = true;
        }
        //computes resetTurnGain, so rotation needed for reorientation corresponds to 360 degrees in VE
        resetTurnGain = 360.0f / Mathf.Abs(resetAngle); 


        if (Mathf.Abs(overallInjectedRotation) < Mathf.Abs(resetAngle))
        {
            float remainingRotation = resetAngle - overallInjectedRotation;

            //If last rotation deltaDir is larger than what is left, we only inject the remaining Rotation and end the reset
            if (Mathf.Abs(remainingRotation) < Mathf.Abs(redirectionManager.deltaDir))
            {
                InjectRotation(remainingRotation);
                redirectionManager.OnResetEnd();
                overallInjectedRotation += remainingRotation;
            }
            else
            {
                //If we turn to the right, deltaDir is positive, so overallInjectedRotation increases
                if(resetDirection > 0)
                {
                    InjectRotation(redirectionManager.deltaDir * (resetTurnGain - 1f)); //if we would only inject the deltadir rotation, it would be a 2:1 turn
                    overallInjectedRotation += redirectionManager.deltaDir; //here only deltaDir, because we check how much the user has rotated in the PE in the first if statement
                }
                //If we turn to the left, deltaDir is negative, so for overallInjectedRotation to increase we have to subtract deltaDir from it
                else
                {
                    InjectRotation(redirectionManager.deltaDir * (resetTurnGain - 1f)); //if we would inject the deltadir rotation, it would be a 2:1 turn
                    overallInjectedRotation -= redirectionManager.deltaDir; //here only deltaDir, because we check how much the user has rotated in the PE in the first if statement
                }
            }
        }
    }
    
    public override void FinalizeReset()
    {
        Destroy(instanceHUD.gameObject); //additional hud for completing
        resetAngleset = false;
        resetVectorset = false;
   }

    public override void SimulatedWalkerUpdate()
    {
        // Act is if there's some dummy target a meter away from you requiring you to rotate
        //redirectionManager.simulatedWalker.RotateIfNecessary(180 - overallInjectedRotation, Vector3.forward);
        redirectionManager.simulatedWalker.RotateInPlace();
        //print("overallInjectedRotation: " + overallInjectedRotation);
    }
    /// <summary>
    /// Sets resetVector to totalForceVector and computes the resetAngle according to the current heading direction
    /// </summary>
    public void GetResetAngleAndDirection()
    {
        if (!resetVectorset)
        {
            resetVector = totalForceVector;
            resetVectorset = true; //prevent resetVector from updating during reset
            //resetAngle = Utilities.GetSignedAngle(redirectionManager.currDirReal, resetVector);

            //now we want to check if the user is stationary, then we will use the gazing direction as reference
            if (redirectionManager.stationaryReal)
            {
                resetAngle = Utilities.GetSignedAngle(redirectionManager.currDirReal, resetVector);
            }
            //if the user is moving, we take the moving direction as reference
            else
                resetAngle = Utilities.GetSignedAngle(redirectionManager.walkingDirectionReal, resetVector) - redirectionManager.gazeAndWalkDifference;

            Debug.Log(("resetVector:" + resetVector, "currRelativeDirection: " + redirectionManager.walkingDirectionReal, "resetAngle: " + resetAngle, "diff: " + redirectionManager.gazeAndWalkDifference));
            resetDirection = (-1) * Mathf.Sign(resetAngle); // -1 because we want to steer the user in the opposite direction of the acute angle
        }
    }

    /// <summary>
    /// Sets HUD according to the resetDirection (anticlockwise, clockwise)
    /// </summary>
    public void SetHUD()
    {
        //if (prefabHUD == null)
            Debug.Log(resetDirection);
            if (resetDirection < 0) //need to use currDirReal
                prefabHUD = Resources.Load<Transform>("S2G Reset HUD ACW");
            if (resetDirection > 0)
                prefabHUD = Resources.Load<Transform>("S2G Reset HUD CW");
        instanceHUD = Instantiate(prefabHUD);
        instanceHUD.parent = redirectionManager.headTransform;
        instanceHUD.localPosition = instanceHUD.position;
        instanceHUD.localRotation = instanceHUD.rotation;
    }
}
