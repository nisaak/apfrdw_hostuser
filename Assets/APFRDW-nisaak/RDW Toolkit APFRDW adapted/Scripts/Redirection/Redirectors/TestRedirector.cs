﻿using System.Collections.Generic;
using UnityEngine;
using Redirection;

/// <summary>
/// This redirector uses the total force vector approach, where each component is derived from the gradient of the underlying potential field.
/// There are a variety of parameters that can be adjusted, such as different thresholds for rotation, translation and dampening methods and factors.
/// Currently when the rotation direction changes signs, it is noticeable and should be dampened more. --> HOW?
/// </summary>
public class TestRedirector : Redirector {

    // Testing Parameters
    private bool useBearingThresholdBasedRotationDampeningTimofey = false;
    private bool useBearingThresholdBasedRotationDampeningNoah = true;
    public bool useSimpleDampening;
    [Range(0.1f, 1f)]
    public float simpleDampening = 0.7f; // simple dampening follows gain *= simpleDampening
    public bool dontUseDampening = true;

    //Initialise and set all parameter variables
    private const float MOVEMENT_THRESHOLD = 0.2f; // meters per second
    private const float ROTATION_THRESHOLD = 1.5f; // degrees per second
    private const float CURVATURE_GAIN_CAP_DEGREES_PER_SECOND = 15;  // degrees per second
    private const float ROTATION_GAIN_CAP_DEGREES_PER_SECOND = 30;  // degrees per second
    private const float DISTANCE_THRESHOLD_FOR_DAMPENING = 1.25f; // Distance threshold to apply dampening (meters)
    //[SerializeField]
    private float BEARING_THRESHOLD_FOR_DAMPENING = 30f; // TIMOFEY: 45.0f; // Bearing threshold to apply dampening (degrees) MAHDI: WHERE DID THIS VALUE COME FROM?
    [Range(0,1)]
    public float SMOOTHING_FACTOR = 0.125f; // Smoothing factor for redirection rotations

    //Initialise variables to keep track of rotations to induce
    public float rotationFromCurvatureGain; //Proposed curvature gain based on user speed
    public float rotationFromRotationGain; //Proposed rotation gain based on head's yaw
    public float rotationFromBaselineRotation;
    private float lastRotationApplied = 0f;
    public float translationFromTranslationGain;
    private bool isStandingStill;

    //reference to redirection vector
    Vector3 totalForceVector;

    UserData userData;

    public TestRedirector(UserData _userData){
        this.userData = _userData;
    }

    public override void ApplyRedirection()
    {
        totalForceVector = userData.totalForceVector;
        Debug.Log(totalForceVector);
        // Get Required Data
        Vector3 deltaPos = userData.deltaPos;
        float deltaDir = userData.deltaDir;

        rotationFromCurvatureGain = 0; //reset all values
        rotationFromRotationGain = 0;
        translationFromTranslationGain = 0;
        rotationFromBaselineRotation = 0;



        //Compute desired facing vector for redirection
        Vector3 desiredFacingDirection = Utilities.FlattenedPos3D(totalForceVector);

        //desired steering direction is positive if we turn clockwise, and negative if anticlockwise (Unity uses left handed system)
        int desiredSteeringDirection = (-1) * (int)Mathf.Sign(Utilities.GetSignedAngle(userData.walkingDirectionReal, desiredFacingDirection)); //i changed currDirReal to walkingdirreal, as the head orientation is not relevant


        //Check if user velocity in PE is > threshold
        if (userData.velocityRealMagnitude > MOVEMENT_THRESHOLD) //if (deltaPos.magnitude / Time.deltaTime > MOVEMENT_THRESHOLD) //User is moving
        {
            isStandingStill = false;

            //g_c is deg/m so we multiply with deltaPosReal to get the rotation in degree
            rotationFromCurvatureGain = Mathf.Rad2Deg * (userData.deltaPosReal.magnitude / userData.CURVATURE_RADIUS);
            rotationFromCurvatureGain = Mathf.Min(rotationFromCurvatureGain, CURVATURE_GAIN_CAP_DEGREES_PER_SECOND * Time.deltaTime);

            //Compute proposed rotation gain
            rotationFromRotationGain = 0;

            //If rotation is above threshold
            if (Mathf.Abs(deltaDir) / Time.deltaTime >= ROTATION_THRESHOLD)  
            {
                //if the sign of desiredsteeringdirection flips, this should avoid the abrupt change in direction, by smoothing it out a bit
                if (Mathf.Approximately(deltaDir * desiredSteeringDirection, 0f))
                {
                    rotationFromRotationGain = 0f;
                }
                //Determine if we need to rotate with or against the user
                if (deltaDir * desiredSteeringDirection < 0)
                {
                    //Rotating against the user
                    rotationFromRotationGain = Mathf.Min(Mathf.Abs(deltaDir * userData.MIN_ROT_GAIN), ROTATION_GAIN_CAP_DEGREES_PER_SECOND * Time.deltaTime);
                }
                else
                {
                    //Rotating with the user
                    rotationFromRotationGain = Mathf.Min(Mathf.Abs(deltaDir * userData.MAX_ROT_GAIN), ROTATION_GAIN_CAP_DEGREES_PER_SECOND * Time.deltaTime);
                }

                //if (deltaDir < Mathf.Deg2Rad * 2.0f)//check if user is rotating
                //{
                //    Vector3 translationGain = transform.forward * (Mathf.Sign(Utilities.GetSignedAngle(totalForceVector, desiredFacingDirection)));
                //}
            }

            //If user is only moving straight and no head rotation or curvature rotation
            else
            {
                //apply translation according to dot product of vectors, if larger then maximum translation gain, else minimum translation gain
                translationFromTranslationGain = Vector3.Dot(Utilities.FlattenedPos3D(totalForceVector),userData.currDirReal) >= 0 ? userData.MAX_TRANS_GAIN : userData.MIN_TRANS_GAIN;   
                // redirectionManager.InjectTranslation(translationFromTranslationGain * userData.deltaPos);
            }

            //Now we find which of the previous rotations (rotation gain, curvature gain) we want to induce to the viewpoint
            float rotationProposed = desiredSteeringDirection * Mathf.Max(rotationFromRotationGain, rotationFromCurvatureGain);

            //evaluates to true if curvature gain is chosen
            bool curvatureGainUsed = rotationFromCurvatureGain > rotationFromRotationGain;

            // Prevent having gains if user is stationary
            if (Mathf.Approximately(rotationProposed, 0))
                return;
            
            //Dampening
            if (!dontUseDampening)
            {
                //DAMPENING METHODS
                // MAHDI: Sinusiodally scaling the rotation when the bearing is near zero
                float bearingToTarget = Vector3.Angle(userData.currDirReal, desiredFacingDirection);
                if (useBearingThresholdBasedRotationDampeningTimofey)
                {
                    // TIMOFEY
                    if (bearingToTarget <= BEARING_THRESHOLD_FOR_DAMPENING)
                        rotationProposed *= Mathf.Sin(Mathf.Deg2Rad * 90 * bearingToTarget / BEARING_THRESHOLD_FOR_DAMPENING);
                }
                if (useBearingThresholdBasedRotationDampeningNoah) //currently is able to damp if obstacle if total force vector points behind the user
                {
                    //if (bearingToTarget < BEARING_THRESHOLD_FOR_DAMPENING)
                    if ((bearingToTarget < BEARING_THRESHOLD_FOR_DAMPENING) ^ (bearingToTarget > (180 - BEARING_THRESHOLD_FOR_DAMPENING / 2)))
                        rotationProposed *= Mathf.Abs(Mathf.Sin(Mathf.Deg2Rad * bearingToTarget));
                }
                else
                {
                    // MAHDI
                    // The algorithm first is explained to be similar to above but at the end it is explained like this. Also the BEARING_THRESHOLD_FOR_DAMPENING value was never mentioned which make me want to use the following even more.
                    rotationProposed *= Mathf.Sin(Mathf.Deg2Rad * bearingToTarget);
                }


                //// MAHDI: Linearly scaling the rotation when the distance is near zero
                //if (desiredFacingDirection.magnitude <= DISTANCE_THRESHOLD_FOR_DAMPENING)
                //{
                //    rotationProposed *= desiredFacingDirection.magnitude / DISTANCE_THRESHOLD_FOR_DAMPENING;
                //}

            }

            // Implement additional rotation with smoothing
            float finalRotation = (1.0f - SMOOTHING_FACTOR) * lastRotationApplied + SMOOTHING_FACTOR * rotationProposed;
            lastRotationApplied = finalRotation;
            if (!curvatureGainUsed){
                // redirectionManager.InjectRotation(finalRotation);
                rotationFromRotationGain = finalRotation;
                rotationFromCurvatureGain = 0;
            }
            else{
                rotationFromCurvatureGain = finalRotation;
                rotationFromRotationGain = 0;
                // redirectionManager.InjectCurvature(finalRotation);
            }

        }

        //If below movement threshold
        else
        {
            if (Mathf.Abs(userData.deltaDirReal) / Time.deltaTime >= ROTATION_THRESHOLD) 
            {
                float rotationProposed;
                if (Mathf.Approximately(userData.deltaDirReal * desiredSteeringDirection, 0f))
                {
                    rotationFromRotationGain = 0f;
                }
                //Determine if we need to rotate with or against the user
                if (userData.deltaDirReal * desiredSteeringDirection < 0)
                {
                    //Rotating against the user
                    rotationFromRotationGain = Mathf.Min(Mathf.Abs(userData.deltaDirReal * userData.MIN_ROT_GAIN), ROTATION_GAIN_CAP_DEGREES_PER_SECOND * Time.deltaTime);
                    rotationFromRotationGain *= desiredSteeringDirection;
                    rotationProposed = rotationFromRotationGain;
                    rotationFromBaselineRotation = 0;
                }
                else
                {
                    //Rotating with the user
                    rotationFromRotationGain = Mathf.Min(Mathf.Abs(userData.deltaDirReal * userData.MAX_ROT_GAIN), ROTATION_GAIN_CAP_DEGREES_PER_SECOND * Time.deltaTime);
                    rotationFromRotationGain *= desiredSteeringDirection;
                    rotationProposed = rotationFromRotationGain;
                    rotationFromBaselineRotation = 0;

                }

                if (!dontUseDampening)
                {
                    //DAMPENING METHODS
                    // MAHDI: Sinusiodally scaling the rotation when the bearing is near zero
                    float bearingToTarget = Vector3.Angle(userData.currDirReal, desiredFacingDirection);
                    if (useBearingThresholdBasedRotationDampeningTimofey)
                    {
                        // TIMOFEY
                        if (bearingToTarget <= BEARING_THRESHOLD_FOR_DAMPENING)
                            rotationProposed *= Mathf.Sin(Mathf.Deg2Rad * 90 * bearingToTarget / BEARING_THRESHOLD_FOR_DAMPENING);
                    }
                    if(useBearingThresholdBasedRotationDampeningNoah) //currently is able to damp if obstacle if total force vector points behind the user
                    {
                        //if (bearingToTarget < BEARING_THRESHOLD_FOR_DAMPENING)
                        if((bearingToTarget < BEARING_THRESHOLD_FOR_DAMPENING) ^ (bearingToTarget > (180-BEARING_THRESHOLD_FOR_DAMPENING/2)))
                            rotationProposed *= Mathf.Abs(Mathf.Sin(Mathf.Deg2Rad * bearingToTarget));
                    }
                    if (useSimpleDampening)
                    {
                        rotationProposed *= simpleDampening;
                    }
                    else
                    {
                        // MAHDI
                        // The algorithm first is explained to be similar to above but at the end it is explained like this. Also the BEARING_THRESHOLD_FOR_DAMPENING value was never mentioned which make me want to use the following even more.
                        rotationProposed *= Mathf.Sin(Mathf.Deg2Rad * bearingToTarget);
                    }


                    //// MAHDI: Linearly scaling the rotation when the distance is near zero
                    //if (desiredFacingDirection.magnitude <= DISTANCE_THRESHOLD_FOR_DAMPENING)
                    //{
                    //    rotationProposed *= desiredFacingDirection.magnitude / DISTANCE_THRESHOLD_FOR_DAMPENING;
                    //}

                }

                float finalRotation = (1.0f - SMOOTHING_FACTOR) * lastRotationApplied + SMOOTHING_FACTOR * rotationProposed;
                lastRotationApplied = finalRotation;

                rotationFromRotationGain = finalRotation;
                rotationFromCurvatureGain = 0;
                rotationFromBaselineRotation = 0;

                // redirectionManager.InjectRotation(rotationFromRotationGain);
            }
            //If standing still inject baseline rotation
            else
            {
                if (userData.deltaDirReal * desiredSteeringDirection < 0)
                {
                    //rotating with the user
                    rotationFromBaselineRotation = userData.BASERATE_ROT * Time.deltaTime;
                    rotationFromBaselineRotation *= desiredSteeringDirection;
                }

                else
                {
                    //rotating against the user
                    rotationFromBaselineRotation = userData.BASERATE_ROT * Time.deltaTime;
                    rotationFromBaselineRotation *= desiredSteeringDirection;
                }

                float rotationProposed = rotationFromBaselineRotation;
                float finalRotation = (1.0f - SMOOTHING_FACTOR) * lastRotationApplied + SMOOTHING_FACTOR * rotationProposed;
                lastRotationApplied = finalRotation;
                // redirectionManager.InjectRotation(rotationFromBaselineRotation);

            }
        }
    }
}
