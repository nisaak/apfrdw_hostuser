﻿using UnityEngine;
using System.Collections;
using Redirection;

public abstract class Redirector : MonoBehaviour //this should be on the redirected user itself
    {

    [HideInInspector]
    public RedirectionManager redirectionManager;

    public GameObject[] staticObstacles;
    public GameObject[] dynamicObstacles;

    /// <summary>
    /// Applies redirection based on the algorithm.
    /// </summary>
    public abstract void ApplyRedirection();

    /// <summary>
    /// Applies rotation to Redirected User. The neat thing about calling it this way is that we can keep track of gains applied.
    /// </summary>
    /// <param name="rotationInDegrees"></param>
    protected void InjectRotation(float rotationInDegrees)
    {
        if (redirectionManager.disableRotation)
        {
            return;
        }
        if (redirectionManager.inReset)
            return;
        else
        {
            if (rotationInDegrees != 0)
            {
                redirectionManager.gameObject.transform.RotateAround(Utilities.FlattenedPos3D(redirectionManager.headTransform.position), Vector3.up, rotationInDegrees);
                redirectionManager.gameObject.GetComponentInChildren<KeyboardController>().SetLastRotation(rotationInDegrees);
                redirectionManager.statisticsLogger.Event_Rotation_Gain(rotationInDegrees / redirectionManager.deltaDir, rotationInDegrees);
            }
        }

    }


    /// <summary>
    /// Applies curvature to Redirected User. The neat thing about calling it this way is that we can keep track of gains applied.
    /// </summary>
    /// <param name="rotationInDegrees"></param>
    protected void InjectCurvature(float rotationInDegrees)
    {
        if (redirectionManager.disableCurvature)
        {
            return;
        }
        else
        {
            if (rotationInDegrees != 0)
            {
                redirectionManager.gameObject.transform.RotateAround(Utilities.FlattenedPos3D(redirectionManager.headTransform.position), Vector3.up, rotationInDegrees);
                redirectionManager.gameObject.GetComponentInChildren<KeyboardController>().SetLastCurvature(rotationInDegrees);
                redirectionManager.statisticsLogger.Event_Curvature_Gain(rotationInDegrees / redirectionManager.deltaPos.magnitude, rotationInDegrees);
            }
        }

    }

    /// <summary>
    /// Applies rotation to Redirected User. The neat thing about calling it this way is that we can keep track of gains applied.
    /// </summary>
    /// <param name="translation"></param>
    protected void InjectTranslation(Vector3 translation)
    {
        if (redirectionManager.disableTranslation)
        {
            return;
        }
        else
        {
            if (translation.magnitude > 0)
            {
                redirectionManager.gameObject.transform.Translate(translation, Space.World);
                redirectionManager.gameObject.GetComponentInChildren<KeyboardController>().SetLastTranslation(translation);
                redirectionManager.statisticsLogger.Event_Translation_Gain(Mathf.Sign(Vector3.Dot(translation, redirectionManager.deltaPos)) * translation.magnitude / redirectionManager.deltaPos.magnitude, Utilities.FlattenedPos2D(translation));
                if (double.IsNaN(Mathf.Sign(Vector3.Dot(translation, redirectionManager.deltaPos)) * translation.magnitude / redirectionManager.deltaPos.magnitude))
                    print("wtf");
            }
        }

    }


    
}
