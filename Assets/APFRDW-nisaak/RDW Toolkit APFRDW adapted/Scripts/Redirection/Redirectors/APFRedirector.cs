﻿using System.Collections.Generic;
using UnityEngine;
using Redirection;

/// <summary>
/// This redirector uses the total force vector approach, where each component is derived from the gradient of the underlying potential field.
/// There are a variety of parameters that can be adjusted, such as different thresholds for rotation, translation and dampening methods and factors.
/// Currently when the rotation direction changes signs, it is noticeable and should be dampened more. --> HOW?
/// </summary>
public class APFRedirector : Redirector {

    // Testing Parameters
    private bool useBearingThresholdBasedRotationDampeningTimofey = false;
    private bool useBearingThresholdBasedRotationDampeningNoah = true;
    public bool useSimpleDampening;
    [Range(0.1f, 1f)]
    public float simpleDampening = 0.7f; // simple dampening follows gain *= simpleDampening
    public bool dontUseDampening = false;

    //Initialise and set all parameter variables
    private const float MOVEMENT_THRESHOLD = 0.2f; // meters per second
    private const float ROTATION_THRESHOLD = 1.5f; // degrees per second
    private const float CURVATURE_GAIN_CAP_DEGREES_PER_SECOND = 15;  // degrees per second
    private const float ROTATION_GAIN_CAP_DEGREES_PER_SECOND = 30;  // degrees per second
    private const float DISTANCE_THRESHOLD_FOR_DAMPENING = 1.25f; // Distance threshold to apply dampening (meters)
    //[SerializeField]
    private float BEARING_THRESHOLD_FOR_DAMPENING = 30f; // TIMOFEY: 45.0f; // Bearing threshold to apply dampening (degrees) MAHDI: WHERE DID THIS VALUE COME FROM?
    [Range(0,1)]
    public float SMOOTHING_FACTOR = 0.125f; // Smoothing factor for redirection rotations

    //Initialise variables to keep track of rotations to induce
    private float rotationFromCurvatureGain; //Proposed curvature gain based on user speed
    private float rotationFromRotationGain; //Proposed rotation gain based on head's yaw
    private float rotationFromBaselineRotation;
    private float lastRotationApplied = 0f;
    private float translationFromTranslationGain;
    private bool isStandingStill;

    //reference to redirection vector

    public APFRedirector(RedirectionManager _redirectionManager){
        this.redirectionManager = _redirectionManager; //set reference
    }


    public override void ApplyRedirection()
    {
        // Get Required Data
        Vector3 deltaPos = redirectionManager.deltaPos;
        float deltaDir = redirectionManager.deltaDir;

        //get total force vector
        rotationFromCurvatureGain = 0;

        //Compute desired facing vector for redirection
        Vector3 desiredFacingDirection = Utilities.FlattenedPos3D(redirectionManager.totalForceVector);

        //desired steering direction is positive if we turn clockwise, and negative if anticlockwise (Unity uses left handed system)
        int desiredSteeringDirection = (-1) * (int)Mathf.Sign(Utilities.GetSignedAngle(redirectionManager.walkingDirectionReal, desiredFacingDirection)); //i changed currDirReal to walkingdirreal, as the head orientation is not relevant


        //Check if user velocity in PE is > threshold
        if (redirectionManager.velocityRealMagnitude > MOVEMENT_THRESHOLD) //if (deltaPos.magnitude / redirectionManager.GetDeltaTime() > MOVEMENT_THRESHOLD) //User is moving
        {
            isStandingStill = false;

            //g_c is deg/m so we multiply with deltaPosReal to get the rotation in degree
            rotationFromCurvatureGain = Mathf.Rad2Deg * (redirectionManager.deltaPosReal.magnitude / redirectionManager.CURVATURE_RADIUS);
            rotationFromCurvatureGain = Mathf.Min(rotationFromCurvatureGain, CURVATURE_GAIN_CAP_DEGREES_PER_SECOND * redirectionManager.GetDeltaTime());

            //Compute proposed rotation gain
            rotationFromRotationGain = 0;

            //If rotation is above threshold
            if (Mathf.Abs(deltaDir) / redirectionManager.GetDeltaTime() >= ROTATION_THRESHOLD)  
            {
                //if the sign of desiredsteeringdirection flips, this should avoid the abrupt change in direction, by smoothing it out a bit
                if (Mathf.Approximately(deltaDir * desiredSteeringDirection, 0f))
                {
                    rotationFromRotationGain = 0f;
                }
                //Determine if we need to rotate with or against the user
                if (deltaDir * desiredSteeringDirection < 0)
                {
                    //Rotating against the user
                    rotationFromRotationGain = Mathf.Min(Mathf.Abs(deltaDir * redirectionManager.MIN_ROT_GAIN), ROTATION_GAIN_CAP_DEGREES_PER_SECOND * redirectionManager.GetDeltaTime());
                }
                else
                {
                    //Rotating with the user
                    rotationFromRotationGain = Mathf.Min(Mathf.Abs(deltaDir * redirectionManager.MAX_ROT_GAIN), ROTATION_GAIN_CAP_DEGREES_PER_SECOND * redirectionManager.GetDeltaTime());
                }

                //if (deltaDir < Mathf.Deg2Rad * 2.0f)//check if user is rotating
                //{
                //    Vector3 translationGain = transform.forward * (Mathf.Sign(Utilities.GetSignedAngle(totalForceVector, desiredFacingDirection)));
                //}
            }

            //If user is only moving straight and no head rotation or curvature rotation
            else
            {
                //apply translation according to dot product of vectors, if larger then maximum translation gain, else minimum translation gain
                translationFromTranslationGain = Vector3.Dot(Utilities.FlattenedPos3D(redirectionManager.totalForceVector),redirectionManager.currDirReal) >= 0 ? redirectionManager.MAX_TRANS_GAIN : redirectionManager.MIN_TRANS_GAIN;   
                InjectTranslation(translationFromTranslationGain * redirectionManager.deltaPos);
            }

            //Now we find which of the previous rotations (rotation gain, curvature gain) we want to induce to the viewpoint
            float rotationProposed = desiredSteeringDirection * Mathf.Max(rotationFromRotationGain, rotationFromCurvatureGain);

            //evaluates to true if curvature gain is chosen
            bool curvatureGainUsed = rotationFromCurvatureGain > rotationFromRotationGain;


            // Prevent having gains if user is stationary
            if (Mathf.Approximately(rotationProposed, 0))
                return;

            //Dampening
            if (!dontUseDampening)
            {
                //DAMPENING METHODS
                // MAHDI: Sinusiodally scaling the rotation when the bearing is near zero
                float bearingToTarget = Vector3.Angle(redirectionManager.currDirReal, desiredFacingDirection);
                if (useBearingThresholdBasedRotationDampeningTimofey)
                {
                    // TIMOFEY
                    if (bearingToTarget <= BEARING_THRESHOLD_FOR_DAMPENING)
                        rotationProposed *= Mathf.Sin(Mathf.Deg2Rad * 90 * bearingToTarget / BEARING_THRESHOLD_FOR_DAMPENING);
                }
                if (useBearingThresholdBasedRotationDampeningNoah) //currently is able to damp if obstacle if total force vector points behind the user
                {
                    //if (bearingToTarget < BEARING_THRESHOLD_FOR_DAMPENING)
                    if ((bearingToTarget < BEARING_THRESHOLD_FOR_DAMPENING) ^ (bearingToTarget > (180 - BEARING_THRESHOLD_FOR_DAMPENING / 2)))
                        rotationProposed *= Mathf.Abs(Mathf.Sin(Mathf.Deg2Rad * bearingToTarget));
                }
                else
                {
                    // MAHDI
                    // The algorithm first is explained to be similar to above but at the end it is explained like this. Also the BEARING_THRESHOLD_FOR_DAMPENING value was never mentioned which make me want to use the following even more.
                    rotationProposed *= Mathf.Sin(Mathf.Deg2Rad * bearingToTarget);
                }


                //// MAHDI: Linearly scaling the rotation when the distance is near zero
                //if (desiredFacingDirection.magnitude <= DISTANCE_THRESHOLD_FOR_DAMPENING)
                //{
                //    rotationProposed *= desiredFacingDirection.magnitude / DISTANCE_THRESHOLD_FOR_DAMPENING;
                //}

            }


            // Implement additional rotation with smoothing
            float finalRotation = (1.0f - SMOOTHING_FACTOR) * lastRotationApplied + SMOOTHING_FACTOR * rotationProposed;
            lastRotationApplied = finalRotation;
            if (!curvatureGainUsed)
                InjectRotation(finalRotation);
            else
                InjectCurvature(finalRotation);


        }

        //If below movement threshold
        else
        {
            if (Mathf.Abs(redirectionManager.deltaDirReal) / redirectionManager.GetDeltaTime() >= ROTATION_THRESHOLD) 
            {
                float rotationProposed;
                if (Mathf.Approximately(redirectionManager.deltaDirReal * desiredSteeringDirection, 0f))
                {
                    rotationFromRotationGain = 0f;
                }
                //Determine if we need to rotate with or against the user
                if (redirectionManager.deltaDirReal * desiredSteeringDirection < 0)
                {
                    //Rotating against the user
                    rotationFromRotationGain = Mathf.Min(Mathf.Abs(redirectionManager.deltaDirReal * redirectionManager.MIN_ROT_GAIN), ROTATION_GAIN_CAP_DEGREES_PER_SECOND * redirectionManager.GetDeltaTime());
                    rotationFromRotationGain *= desiredSteeringDirection;
                    rotationProposed = rotationFromRotationGain;

                }
                else
                {
                    //Rotating with the user
                    rotationFromRotationGain = Mathf.Min(Mathf.Abs(redirectionManager.deltaDirReal * redirectionManager.MAX_ROT_GAIN), ROTATION_GAIN_CAP_DEGREES_PER_SECOND * redirectionManager.GetDeltaTime());
                    rotationFromRotationGain *= desiredSteeringDirection;
                    rotationProposed = rotationFromRotationGain;
                }

                if (!dontUseDampening)
                {
                    //DAMPENING METHODS
                    // MAHDI: Sinusiodally scaling the rotation when the bearing is near zero
                    float bearingToTarget = Vector3.Angle(redirectionManager.currDirReal, desiredFacingDirection);
                    if (useBearingThresholdBasedRotationDampeningTimofey)
                    {
                        // TIMOFEY
                        if (bearingToTarget <= BEARING_THRESHOLD_FOR_DAMPENING)
                            rotationProposed *= Mathf.Sin(Mathf.Deg2Rad * 90 * bearingToTarget / BEARING_THRESHOLD_FOR_DAMPENING);
                    }
                    if(useBearingThresholdBasedRotationDampeningNoah) //currently is able to damp if obstacle if total force vector points behind the user
                    {
                        //if (bearingToTarget < BEARING_THRESHOLD_FOR_DAMPENING)
                        if((bearingToTarget < BEARING_THRESHOLD_FOR_DAMPENING) ^ (bearingToTarget > (180-BEARING_THRESHOLD_FOR_DAMPENING/2)))
                            rotationProposed *= Mathf.Abs(Mathf.Sin(Mathf.Deg2Rad * bearingToTarget));
                    }
                    if (useSimpleDampening)
                    {
                        rotationProposed *= simpleDampening;
                    }
                    else
                    {
                        // MAHDI
                        // The algorithm first is explained to be similar to above but at the end it is explained like this. Also the BEARING_THRESHOLD_FOR_DAMPENING value was never mentioned which make me want to use the following even more.
                        rotationProposed *= Mathf.Sin(Mathf.Deg2Rad * bearingToTarget);
                    }


                    //// MAHDI: Linearly scaling the rotation when the distance is near zero
                    //if (desiredFacingDirection.magnitude <= DISTANCE_THRESHOLD_FOR_DAMPENING)
                    //{
                    //    rotationProposed *= desiredFacingDirection.magnitude / DISTANCE_THRESHOLD_FOR_DAMPENING;
                    //}

                }

                float finalRotation = (1.0f - SMOOTHING_FACTOR) * lastRotationApplied + SMOOTHING_FACTOR * rotationProposed;
                lastRotationApplied = finalRotation;

                InjectRotation(rotationFromRotationGain);
            }
            //If standing still inject baseline rotation
            else
            {
                if (redirectionManager.deltaDirReal * desiredSteeringDirection < 0)
                {
                    //rotating with the user
                    rotationFromBaselineRotation = redirectionManager.BASERATE_ROT * redirectionManager.GetDeltaTime();
                    rotationFromBaselineRotation *= desiredSteeringDirection;
                }

                else
                {
                    //rotating against the user
                    rotationFromBaselineRotation = redirectionManager.BASERATE_ROT * redirectionManager.GetDeltaTime();
                    rotationFromBaselineRotation *= desiredSteeringDirection;
                }

                float rotationProposed = rotationFromBaselineRotation;
                float finalRotation = (1.0f - SMOOTHING_FACTOR) * lastRotationApplied + SMOOTHING_FACTOR * rotationProposed;
                lastRotationApplied = finalRotation;
                InjectRotation(rotationFromBaselineRotation);

            }
        }
    }
}
