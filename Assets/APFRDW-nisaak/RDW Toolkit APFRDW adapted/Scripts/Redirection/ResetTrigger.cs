﻿using UnityEngine;
using System.Collections;
/// <summary>
/// this class is reponsible for defining the size of the collider, i.e. the size of the tracking room and the extents to which a user can walk without triggering a reset
/// </summary>
public class ResetTrigger : MonoBehaviour {

    [HideInInspector]
    public RedirectionManager redirectionManager;
    public Collider bodyCollider;

    [SerializeField, Range(0f, 1f)]
    public float RESET_TRIGGER_BUFFER;

    [HideInInspector]
    public float xLength, zLength;

    public void Initialize()
    {
        // Set Size of Collider
        float trimAmountOnEachSide_x = RESET_TRIGGER_BUFFER; //float trimAmountOnEachSide_x = bodyCollider.transform.localScale.x + 2 * RESET_TRIGGER_BUFFER;  
        float trimAmountOnEachSide_z = RESET_TRIGGER_BUFFER; //float trimAmountOnEachSide_z = bodyCollider.transform.localScale.z + 2 * RESET_TRIGGER_BUFFER;
        this.transform.localScale = new Vector3(1 - (trimAmountOnEachSide_x / this.transform.parent.localScale.x), 2 / this.transform.parent.localScale.y, 1 - (trimAmountOnEachSide_z / this.transform.parent.localScale.z));
        xLength = this.transform.parent.localScale.x - trimAmountOnEachSide_x;
        zLength = this.transform.parent.localScale.z - trimAmountOnEachSide_z;
    }

    void OnTriggerEnter(Collider other)
    {

    }

    void OnTriggerExit(Collider other)
    {
        if (other == bodyCollider)
        {
            redirectionManager.OnResetTrigger();
        }
    }

    

}
