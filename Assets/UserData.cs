﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*Userdata contains all data the user outputs for the host to use*/

public class UserData : MonoBehaviour
{
    [HideInInspector]
    public float MAX_TRANS_GAIN; //make so that it updates
    [HideInInspector]
    public float MIN_TRANS_GAIN;
    [HideInInspector]
    public float MAX_ROT_GAIN;
    [HideInInspector]
    public float MIN_ROT_GAIN;
    [HideInInspector]
    public float CURVATURE_RADIUS;
    [HideInInspector]
    public float BASERATE_ROT;

    public int userIndex;
    [HideInInspector]
    public Vector3 currPos, currPosReal, prevPos, prevPosReal;
    [HideInInspector]
    public Vector3 currDir, currDirReal, prevDir, prevDirReal;

    //this captures the deltaPos deltaDir in virtual and physical space
    [HideInInspector]
    public Vector3 deltaPos, deltaPosReal; 
    [HideInInspector]
    public float deltaDir, deltaDirReal;

    //this captures the current walkingDirection in virtual and physical space
    [HideInInspector]
    public Vector3 walkingDirection, walkingDirectionReal;

    //vcetor3 for velocity vector in virtual and physical space
    [HideInInspector]
    public Vector3 velocity, velocityReal;
    [HideInInspector]
    public float velocityMagnitude, velocityRealMagnitude;
    [HideInInspector]
    public bool stationaryReal;
    [HideInInspector]
    public float gazeAndWalkDifference;

    [HideInInspector]
    public Vector3 totalForceVector;

}
    
