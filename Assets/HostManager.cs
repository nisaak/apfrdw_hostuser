﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

/*The host manager is responsible for keeping track of all users, receiving their Userdata, computing the gains and writing the data to each of the hostdata containers*/
public class HostManager : MonoBehaviour
{
    List<TestRedirector> testRedirectors = new List<TestRedirector>(); //List of all redirectors, one for each RedUser where the gains are computed
    public List<HostData> hostDataList = new List<HostData>(); //create list to hold hostdata for each of the user
    public List<UserData> userDataList = new List<UserData>();
    List<GameObject> userList = new List<GameObject>();
    
    private void Awake() {
        userList = GameObject.FindGameObjectsWithTag("redUser").ToList();
        InitializeHostData();
        InitializeUserData();
        InitializeRedirectors();
    }
    
    void Start()
    {
    }

    void InitializeHostData(){
        int index = 0;
        foreach(GameObject user in userList){
            HostData hostData = new HostData();
            hostDataList.Add(hostData); //index may help as well
            hostDataList[index].userIndex = index;
            index++;
        }
    }

    
    void InitializeUserData(){
        int index = 0;
        foreach(GameObject user in userList){
            UserData userData = user.GetComponent<UserData>(); //this should be networked
            userDataList.Add(userData); //index may help as well
            userDataList[index].userIndex = index;
            index++;
        }
    }

    void InitializeRedirectors(){
        int index = 0;
        foreach(GameObject user in userList){
            TestRedirector testRedirector = new TestRedirector(userDataList[index]);
            testRedirectors.Add(testRedirector);
            index++;
        }
    }

    void WriteHostData(){
        for (int index = 0; index < hostDataList.Count; index++){   
            hostDataList[index].curvatureGain = testRedirectors[index].rotationFromCurvatureGain;
            hostDataList[index].rotationGain = testRedirectors[index].rotationFromRotationGain;
            hostDataList[index].translationGain = testRedirectors[index].translationFromTranslationGain;
            hostDataList[index].baselineGain = testRedirectors[index].rotationFromBaselineRotation;
        }
    }



    // Update is called once per frame
    void Update()
    {
        WriteHostData();
        for (int index = 0; index < testRedirectors.Count; index++)
        {
            testRedirectors[index].ApplyRedirection();
        }
    }
}
