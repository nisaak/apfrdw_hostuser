﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameSetup : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        CreatePlayer();
    }

    public void CreatePlayer(){
        Debug.Log("Create Player");
        if(PhotonNetwork.IsMasterClient){
            PhotonNetwork.Instantiate("Host", Vector3.zero, Quaternion.identity,0);
        }
        else{
            PhotonNetwork.Instantiate("redUser", Vector3.zero, Quaternion.identity,0);
        }
    }
}
