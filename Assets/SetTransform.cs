﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetTransform : MonoBehaviour
{
    GameObject trackedSpace; //reference to tracked space, as it will provide the origin

    private void Awake()
    {
        this.transform.localPosition = new Vector3(-0.35f, 0f, 1.57f);
        this.transform.localRotation = Quaternion.identity;
    }
    // Update is called once per frame
    void Update()
    {

    }
}
