﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*hostdata contains all data the host outputs for the user*/
public class HostData
{       
    public int userIndex;
    public float translationGain;
    public float curvatureGain;
    public float rotationGain;
    public float baselineGain;

}
