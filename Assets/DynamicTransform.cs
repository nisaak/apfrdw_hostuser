﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class DynamicTransform : MonoBehaviour
{

    GameObject controller;

    private void Awake()
    {
        controller = GameObject.Find("Controller (right)");
    }
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.GetComponent<MeshRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position = controller.transform.position;
        
    }
}
